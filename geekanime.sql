-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 19 juin 2020 à 01:16
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `geekanime`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `adresseID` int(11) NOT NULL AUTO_INCREMENT,
  `codePostal` int(11) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `numeroVoie` int(11) NOT NULL,
  `nomRue` varchar(255) NOT NULL,
  PRIMARY KEY (`adresseID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `categorieID` int(11) NOT NULL AUTO_INCREMENT,
  `nomCategorie` varchar(255) NOT NULL,
  `descriptionCategorie` text NOT NULL,
  PRIMARY KEY (`categorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`categorieID`, `nomCategorie`, `descriptionCategorie`) VALUES
(1, 'Accessoire ', 'Pour les avoir des souris clavier yakuta etc les accessoires otaku '),
(2, 'Katana', 'Pour les personnes qui aime les otaku '),
(3, 'Manga/Anime', 'Ensemble des manga et anime '),
(4, 'Jeu vidéo', 'Un jeu vidéo est une activité de loisir basée sur des périphériques informatiques (écran LCD, manette/joystick, hauts parleurs, ...) permettant d\'interagir dans un environnement virtuel conformément à un ensemble de règles prédéfinies');

-- --------------------------------------------------------

--
-- Structure de la table `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `questionID` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `commentaires` varchar(255) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`questionID`),
  KEY `faq_ibfk_1` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `faq`
--

INSERT INTO `faq` (`questionID`, `question`, `commentaires`, `userID`) VALUES
(6, 'Comment acheter sur notre site ?', 'voila j,ai cherche partout mais pas d\'idee', 24);

-- --------------------------------------------------------

--
-- Structure de la table `faqreponses`
--

DROP TABLE IF EXISTS `faqreponses`;
CREATE TABLE IF NOT EXISTS `faqreponses` (
  `reponseID` int(11) NOT NULL AUTO_INCREMENT,
  `reponse` varchar(255) NOT NULL,
  `userID` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  PRIMARY KEY (`reponseID`),
  KEY `faqreponses_ibfk_1` (`userID`),
  KEY `faqreponses_ibfk_2` (`questionID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `lignepanier`
--

DROP TABLE IF EXISTS `lignepanier`;
CREATE TABLE IF NOT EXISTS `lignepanier` (
  `lignePanierID` int(11) NOT NULL AUTO_INCREMENT,
  `panierID` int(11) NOT NULL,
  `numeroLignePanier` int(11) NOT NULL,
  `produitID` int(11) NOT NULL,
  `quantité` int(11) NOT NULL,
  PRIMARY KEY (`lignePanierID`),
  KEY `panierID` (`panierID`),
  KEY `produitID` (`produitID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `lignepanier`
--

INSERT INTO `lignepanier` (`lignePanierID`, `panierID`, `numeroLignePanier`, `produitID`, `quantité`) VALUES
(4, 5, 1, 22, 1),
(5, 8, 1, 1, 1),
(6, 8, 2, 3, 1),
(7, 8, 3, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `moyendepaiement`
--

DROP TABLE IF EXISTS `moyendepaiement`;
CREATE TABLE IF NOT EXISTS `moyendepaiement` (
  `moyenDePaiementID` int(11) NOT NULL AUTO_INCREMENT,
  `nomMoyenDePaiement` varchar(255) NOT NULL,
  `descriptionMoyenDePaiement` text NOT NULL,
  PRIMARY KEY (`moyenDePaiementID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `moyendepaiement`
--

INSERT INTO `moyendepaiement` (`moyenDePaiementID`, `nomMoyenDePaiement`, `descriptionMoyenDePaiement`) VALUES
(1, 'Effets de commerce', 'Les effets de commerce tels que la traite (ou lettre de change) et le billet à ordre, instruments tant de crédit que de paiement.'),
(2, 'Chèque ', 'Le chèque est un moyen de paiement scriptural utilisant le circuit bancaire. Il est généralement utilisé pour faire transiter de la monnaie d\'un compte bancaire à un autre. Tombé en désuétude dans la plupart des pays industrialisés, il reste encore souvent utilisé en France, au Royaume-Uni et aux États-Unis.'),
(3, 'Coupon de paiement', 'Le coupon de paiement, ticket d\'achat vendu notamment par les buralistes, permettant de recharger des cartes bancaires prépayées ; utilisé notamment pour des créditer une compte de jeux en ligne. Parce qu\'il est également objet de nombreuses fraudes du fait de son caractère anonyme, la directive sur le service des paiements vise a réduire le montant maximum journalier.'),
(4, 'Porte-monnaie électronique', 'Le porte-monnaie électronique est un dispositif qui peut stocker de la monnaie sans avoir besoin d\'un compte bancaire et d\'effectuer directement des paiements sur des terminaux de paiement.'),
(5, 'Crypto-monnaie', 'Une crypto-monnaie ou monnaie cryptographique est une monnaie électronique sur un réseau informatique pair à pair ou décentralisée basé sur les principes de la cryptographie pour valider les transactions et émettre la monnaie elle-même1,2. Aujourd\'hui, toutes les crypto-monnaies sont des monnaies alternatives, car elles n\'ont de cours légal dans aucun pays. Les crypto-monnaies utilisent un système de preuve de travail pour les protéger des contrefaçons électroniques. De nombreuses crypto-monnaies ont été développées mais la plupart sont similaires et dérivent de la première implémentation complète : le Bitcoin.'),
(6, 'Carte de paiement', 'Une carte de paiement est un moyen de paiement se présentant sous la forme d\'une carte plastique mesurant 85,60 × 53,98 mm, équipée d\'une bande magnétique et/ou puce électronique (c\'est alors une carte à puce), et qui permet :\r\n\r\nle paiement, auprès de commerces physiques possédant un terminal de paiement électronique ou auprès de commerces virtuels via Internet ;\r\nles retraits d\'espèces aux distributeurs de billets.\r\nLa carte de paiement est associée à un réseau de paiement, tel que VISA, MasterCard, American Express, JCB, le Groupe Carte Bleue. Une carte de paiement peut être à « débit immédiat », à débit différé ou une carte de crédit.\r\n\r\nLe réseau interbancaire français possède une particularité : toute carte disposant de la marque « CB - Carte bancaire » permet de payer par le biais du réseau interbancaire français, le Groupement des Cartes Bancaires CB.');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `panierID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `etatPanier` int(11) NOT NULL,
  `adresseID` int(11) DEFAULT NULL,
  `moyenDePaiementID` int(11) DEFAULT NULL,
  `HeureAchat` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`panierID`),
  KEY `userID` (`userID`),
  KEY `adresseID` (`adresseID`),
  KEY `moyenDePaiementID` (`moyenDePaiementID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`panierID`, `userID`, `etatPanier`, `adresseID`, `moyenDePaiementID`, `HeureAchat`) VALUES
(8, 26, 1, NULL, 6, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `produitID` int(11) NOT NULL AUTO_INCREMENT,
  `nomProduit` varchar(255) COLLATE utf8_bin NOT NULL,
  `prix` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `cheminimage` varchar(500) COLLATE utf8_bin NOT NULL,
  `sousCategorieID` int(11) NOT NULL,
  PRIMARY KEY (`produitID`),
  KEY `sousCategorieID` (`sousCategorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`produitID`, `nomProduit`, `prix`, `description`, `cheminimage`, `sousCategorieID`) VALUES
(1, 'tasse gamer', 4, 'Voici une tasse pour gamer ', 'Images/Produit/1.jpg', 4),
(2, 'clavier gamer ', 60, 'Voici un clavier gamer avec led color', 'Images/Produit/2.jpg', 1),
(3, 'Porte cle Playstation ', 2, 'Voici un porte cle qui va etre parfait pour vous ', 'Images/Produit/3.jpg', 5),
(4, 'Porte cle ', 2, 'Izuku sera avec vous dans vos aventure ', 'Images/Produit/4.jpg', 5),
(5, 'Tasse Sony ', 3, 'Voila la tasse Playstation a vous service ', 'Images/Produit/5.png', 5),
(6, 'Black clover dvd', 10, 'Black clover est de retour avec un pack pour regarde les épisode', 'Images/Produit/6.png', 3),
(7, 'Boku no hero livre ', 6, 'Boku no  hero pour les fans des aventure du hero izuku ', 'Images/Produit/7.jpg', 7),
(8, 'Bokuto gintoki', 70, 'Voici un bokuto du fameux gintoki dans gintama fais a la main ', 'Images/Produit/8.jpg', 6),
(9, 'City hunter dvd', 10, 'Vous voulez voir un classique city hunter et fait pour vous ', 'Images/Produit/9.jpg', 3),
(10, 'Cyberpunk ', 70, 'Vous pouvez déjà réservé cyber2022', 'Images/Produit/10.jpg', 4),
(11, 'DBZ super', 5, 'Goku est de retour avec les dieu dans dragon ball super', 'Images/Produit/11.jpg', 7),
(12, 'FFVII', 69, 'Voila le fameux remake de final fantasy 7 ', 'Images/Produit/12.jpg', 4),
(13, 'Fairy tail dvd', 10, 'Le dvd fairy tail est la vous pouvez regard natsu et c est amis ', 'Images/Produit/13.jpg', 3),
(14, 'ghost', 70, 'Vous pouvez reserve le fameux ghost of tsushima ', 'Images/Produit/14.jpg', 4),
(15, 'haikyu', 5, 'Haikyu est de pour voir le fameux hinta dans le volley ball ', 'Images/Produit/15.jpg', 7),
(16, 'Jump', 6, 'Tous les actualite manga anime jeux manga est sur ce jameux livre jump ', 'Images/Produit/16.jpg', 7),
(17, 'Kingdom', 5, 'Le manga kingdom est la ', 'Images/Produit/17.jpg', 7),
(18, 'clavier gamer ', 80, 'Voici un clavier gamer avec le led + un pose iphone ', 'Images/Produit/18.jpg', 1),
(19, 'Magi ', 5, 'Magi est pour le monde a coupe le souffle ', 'Images/Produit/19.jpg', 7),
(20, 'Katana ', 150, 'Un katana a mettre dans un emplacement dédié ', 'Images/Produit/20.jpg', 6),
(21, 'One piece', 5, 'Luffy est son équipage est la pour une aventure dans mer comme pirate ', 'Images/Produit/21.jpg', 7),
(22, 'porte cle naruto ', 3, 'Voila le porte cle de naruto en sennin ', 'Images/Produit/22.jpg', 2),
(23, 'Katana en bois ', 180, 'Voici un katana en bois avec une lame ', 'Images/Produit/23.jpg', 6),
(24, 'Katana ', 200, 'deux katana en bois avec une lame l\'autre katana traditionnel ', 'Images/Produit/24.jpg', 6);

-- --------------------------------------------------------

--
-- Structure de la table `souscategorie`
--

DROP TABLE IF EXISTS `souscategorie`;
CREATE TABLE IF NOT EXISTS `souscategorie` (
  `sousCategorieID` int(11) NOT NULL AUTO_INCREMENT,
  `nomSousCategorie` varchar(255) NOT NULL,
  `descriptionSousCategorie` text NOT NULL,
  `categorieID` int(11) NOT NULL,
  PRIMARY KEY (`sousCategorieID`),
  KEY `categorieID` (`categorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `souscategorie`
--

INSERT INTO `souscategorie` (`sousCategorieID`, `nomSousCategorie`, `descriptionSousCategorie`, `categorieID`) VALUES
(1, 'Accessoires', 'Manettes, batteries, multiprises, connectique, tout ce qu\'il faut pour faire fonctionner consoles et ordinateurs', 4),
(2, 'Accessoires', 'Accessoires otaku manga ', 1),
(3, 'Anime', 'Anime dvd ', 3),
(4, 'Jeux', 'Jeux vidéos', 4),
(5, 'Autre', 'tasses,figurine,carte,porte cle , etc ', 1),
(6, 'Katana', 'Katana otaku ', 2),
(7, 'Manga', 'livre de manga jump otaku etc', 3);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `chemin` varchar(500) COLLATE utf8_bin NOT NULL,
  `niveau_accreditation` int(11) NOT NULL,
  `mail` varchar(255) COLLATE utf8_bin NOT NULL,
  `mot_de_passe` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`userID`, `nom`, `prenom`, `chemin`, `niveau_accreditation`, `mail`, `mot_de_passe`) VALUES
(24, 'sasa', 'zouka', 'Images/Profil/profil_utilisateur.jpg', 2, 'nin@outlook.fr', '4743fa1cbe9f43cf2877eb9776615e38c9f4653f'),
(25, 'ninja', 'ninja', 'Images/Profil/profil_utilisateur.jpg', 1, 'ninja@hotmail.com', 'a210bea55a0b7514662f536f9b0eabb3ebf94df4'),
(26, 'sav', 'sav', 'Images/Profil/profil_utilisateur.jpg', 2, 'sav@hotmail.com', 'a210bea55a0b7514662f536f9b0eabb3ebf94df4'),
(27, 'sonic', 'sonic', 'Images/Profil/profil_utilisateur.jpg', 2, 'sonic@sonic.com', '9dc3887df095899a876afcbdc6877a3e72295892'),
(28, 'zola', 'sonic', 'Images/Profil/profil_utilisateur.jpg', 1, 'zola@gmail.com', 'a210bea55a0b7514662f536f9b0eabb3ebf94df4');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`);

--
-- Contraintes pour la table `faqreponses`
--
ALTER TABLE `faqreponses`
  ADD CONSTRAINT `faqreponses_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `faqreponses_ibfk_2` FOREIGN KEY (`questionID`) REFERENCES `faq` (`questionID`);

--
-- Contraintes pour la table `lignepanier`
--
ALTER TABLE `lignepanier`
  ADD CONSTRAINT `lignepanier_ibfk_1` FOREIGN KEY (`panierID`) REFERENCES `panier` (`panierID`),
  ADD CONSTRAINT `lignepanier_ibfk_2` FOREIGN KEY (`produitID`) REFERENCES `produit` (`produitID`);

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `panier_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `panier_ibfk_2` FOREIGN KEY (`adresseID`) REFERENCES `adresse` (`adresseID`),
  ADD CONSTRAINT `panier_ibfk_3` FOREIGN KEY (`moyenDePaiementID`) REFERENCES `moyendepaiement` (`moyenDePaiementID`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`sousCategorieID`) REFERENCES `souscategorie` (`sousCategorieID`);

--
-- Contraintes pour la table `souscategorie`
--
ALTER TABLE `souscategorie`
  ADD CONSTRAINT `souscategorie_ibfk_1` FOREIGN KEY (`categorieID`) REFERENCES `categorie` (`categorieID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
