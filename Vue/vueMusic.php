<section id="ij" >
<div class="player">
    <div class="player-track">
      <div class="artist-name"></div>
      <div class="music-name"></div>
      <div class="progress-bar">
        <div class="fillBar"></div>
      </div>
      <div class="time"></div>
    </div>
    </div>
    <div class="player-control">
      <i id="prev" class="fas fa-backward"></i>
      <i id="play" class="fas fa-play"></i>
      <i id="next" class="fas fa-forward"></i>
    </div>
    
    </section>

