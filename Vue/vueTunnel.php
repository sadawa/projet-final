<?php
        require 'Vue/vueModTunnel.php';
    ?>

    <table>
    <?php
    if (empty($lignepanier_line = $lignepanier->fetch())) {
        echo "Votre panier est vide";
    } else {
        while ($lignepanier_line) {
            ?>
            <tr>
                <td>
                    Produit: <?php echo $lignepanier_line["nomProduit"] ?>
                </td>
                <td> |
                </td>
                <td>
                    <img height="100px" src="<?php echo $lignepanier_line["cheminimage"] ?>"/>
                </td>
                <td> |
                </td>
                <td>
                    Quantité: <?php echo $lignepanier_line["quantité"] ?>
                </td>
                <td> |
                </td>
                <td>
                    Prix unitaire: <?php echo $lignepanier_line["prix"] ?>€
                </td>
                <td> |
                </td>
                <td>
                    Prix total: <?php echo $lignepanier_line["prix"] * $lignepanier_line["quantité"] ?>€
                </td>
            </tr>
            <?php
            $lignepanier_line = $lignepanier->fetch();
        }
        ?>
        </table>

        <form method="POST" action="?action=tunnel">
            <table>

                <tr>
                    <td>Numéro de rue</td>
                    <td><input type="text" name="number"/></td>
                </tr>
                <tr>
                    <td>Adresse</td>
                    <td><input type="text" name="adress"/></td>
                </tr>
                <tr>
                    <td>Ville</td>
                    <td><input type="text" name="city"/></td>
                </tr>
                <tr>
                    <td>Code postal</td>
                    <td><input type="text" name="code"/></td>
                </tr>
            </table>

          

            <label for="paiement">
                Moyen de paiemment:
                <span class="custom-dropdown custom-dropdown--white">
			<select name="paiemment" id="paiement" class="custom-dropdown__select custom-dropdown__select--white">
				<?php
                while ($paiemment_line = $paiemment->fetch()) {
                    ?>
                    <option><?php echo $paiemment_line["nomMoyenDePaiement"] ?></option>
                    <?php
                }
                ?>
			</select>
		</span>
            </label>
            <?php
            if (isset($_POST['paiemment'])) {
                $moyenPaiemment = $_POST['paiemment'];
            }
            ?>
          
            <input value="Valider" href="index.php?action=tunnel" type="submit">
        </form>

        <?php
        require 'Vue/vueAdressTunnel.php';
    }

?>