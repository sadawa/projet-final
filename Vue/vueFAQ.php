
<?php if (isset($_GET['question'])) { ?>
    <div class="FAQ">
        <h1>Question : <?php echo $listQuestions['question'] ?></h1>
        <h5>par : <?php echo $listQuestions['prenom'] . ' ' . $listQuestions['nom'] ?></h5>
        <p><h4>Commentaires :</h4><?php echo $listQuestions['commentaires'] ?></p>
        <?php foreach ($listReponses as $rep) { ?>
            <h4>Réponse de : <?php echo $rep['prenom'] . ' ' . $rep['nom'] ?></h4>
            <p><?php echo $rep['reponse'] ?></p>
        <?php } ?>
        </br>
        <h4>Répondez à <?php echo $listQuestions['prenom'] . ' ' . $listQuestions['nom'] ?></h4>
        <form action="index.php?action=faq&question=<?php echo $listQuestions['questionID'] ?>" method="post">
            <textarea name="reponse" required="required"></textarea>
            <input type="submit" name="sbButton3">
        </form>
    </div>
<?php } else { ?>
    <div class="subfaq">
        <div class="FAQ">
            <h2>Bienvenue sur la page FAQ</h2>
            <section class="recherche">
            <form action="index.php?action=faq" method="post">
                <input type="text" name="research" placeholder="Rechercher" class="recherche" required="required">
                <input type="submit" name="sbButton">
            </form>
        </section>
            <ul>
                <?php if (!empty($listQuestions)) {
                    foreach ($listQuestions as $question) { ?>
                        <li>
                            <a href="index.php?action=faq&question=<?php echo $question['questionID'] ?>"><?php echo $question['question'] ?></a>
                        </li>
                    <?php }
                } else { ?>
                    <h4>Aucune question ne correspond à cette recherche</h4>
                <?php } ?>
            </ul>
        </div>
        <div class="FAQ">
        <section class="question">
            <form action="index.php?action=faq" method="post">
                <h3>Posez votre question :</h3>
                <div class="faq">
                   <h4> Question :</h4>
                    <input type="text" name="question"  required="required">
                </div>
                <div class="faq">
                   <h4> Commentaires : <h4>
                    <textarea name="commentaires" class="com"  required="required"></textarea>
                </div>
                <input type="submit" name="sbButton2">
            </form>
            </section>
    </div>
<?php } ?>