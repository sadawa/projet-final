<?php


//________________________________________________________________________________________
// Require once
require_once('Controleur.php');
require_once('Vue/Vue.php');

class ControleurTunnel implements Controleur
{
    //______________________________________________________________________________________
    /**
     * ControleurTunnel constructor.
     */
    public function __construct()
    {

    }


    //______________________________________________________________________________________
    /**
     * Affiche la page tunnel
     */
    public function getHTML()
    {
        $vue = new Vue("Tunnel");
        $vue->generer(array());
    }
}