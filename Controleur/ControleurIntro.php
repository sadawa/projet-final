<?php

//________________________________________________________________________________________
// Require once
require_once('Controleur/Controleur.php');
require_once('Vue/Vue.php');

class ControleurIntro implements Controleur
{
    //______________________________________________________________________________________
    /**
     * ControleurAccueil constructor.
     */
    public function __construct()
    {

    }


    //______________________________________________________________________________________
    /**
     *  Affiche la vue de la page
     */
    public function getHTML()
    {
        $vue = new Vue("Intro");
        $vue->generer(array());
    }
}