<?php


//________________________________________________________________________________________
// Require once
require_once('Controleur/Controleur.php');
require_once('Vue/Vue.php');

class ControleurMusic implements Controleur
{
    //______________________________________________________________________________________
    /**
     * ControleurMusic constructor.
     */
    public function __construct()
    {

    }


    //______________________________________________________________________________________
    /**
     * Affiche la page music
     */
    public function getHTML()
    {
        $vue = new Vue("Music");
        $vue->generer(array());
    }
}