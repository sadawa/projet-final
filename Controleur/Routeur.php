<?php
session_start();

//________________________________________________________________________________________
// Require once
require_once('Vue/Vue.php');
require_once('Controleur/Controleur.php');
require_once('Controleur/ControleurAccueil.php');
require_once('Controleur/ControleurUserProfile.php');
require_once('Controleur/ControleurInscription.php');
require_once('Controleur/ControleurAdministrationUser.php');
require_once('Controleur/ControleurAdministrationHistoriqueCommande.php');
require_once('Controleur/ControleurAdministrationProduit.php');
require_once('Controleur/ControleurAdministrationPaiementLivraison.php');
require_once('Controleur/ControleurProductCategorie.php');
require_once('Controleur/ControleurProduitList.php');
require_once('Controleur/ControleurTunnel.php');
require_once('Controleur/ControleurLogin.php');
require_once('Controleur/ControleurRecherche.php');
require_once('Controleur/ControleurChiffreAffaire.php');
require_once('Controleur/ControleurFAQ.php');
require_once('Controleur/ControleurMusic.php');
require_once('Controleur/ControleurIntro.php');

//________________________________________________________________________________________
// Class
class Routeur
{
    // Attributs
    private $ctrlAccueil;
    private $ctrlUserProfile;
    private $ctrlAdminProduit;
    private $ctrlAdminHistoriqueCommande;
    private $ctrlAdminUser;
    private $ctrlProductCategorie;
    private $ctrlProduitList;
    private $ctrlTunnel;
    private $ctrlRecherche;
    private $ctrlChiffreAffaire;
    private $ctrlFAQ;
    private $ctrlMusic;
    private $ctrlIntro;


    //______________________________________________________________________________________
    /**
     * Routeur constructor.
     */
    public function __construct()
    {
        $this->ctrlAccueil = new ControleurAccueil();
        $this->ctrlUserProfile = new ControleurUserProfile();
        $this->ctrlProductCategorie = new ControleurProductCategorie();
        $this->ctrlProduitList = new ControleurProduitList();
        $this->ctrlInscription = new ControleurInscription();
        $this->ctrlAdminProduit = new ControleurAdministrationProduit();
        $this->ctrlAdminUser = new ControleurAdministrationUser();
        $this->ctrlAdminPaiementLivraison = new ControleurAdministrationPaiementLivraison();
        $this->ctrlAdminHistoriqueCommande = new ControleurAdministrationHistoriqueCommande();
        $this->ctrlTunnel = new ControleurTunnel();
        $this->ctrlLogin = new ControleurLogin();
        $this->ctrlRecherche = new ControleurRecherche();
        $this->ctrlChiffreAffaire = new ControleurChiffreAffaire();
        $this->ctrlFAQ = new ControleurFAQ();
        $this->ctrlMusic = new ControleurMusic();
        $this->ctrlIntro = new ControleurIntro();
    }

    /**
     * Fonction qui traite une requête entrante en fonction de l'action
     */
    public function routerRequete()
    {
        try {
            if (isset($_GET['action'])) {
               switch($_GET['action']){
                    case 'accueil':
                    $this->ctrlAccueil->getHTML();
                    break;

                    case 'userProfile':
                    $this->ctrlUserProfile->handlerUserProfile();
                    break;

                    case 'productCategorie':
                    $this->ctrlProductCategorie->getHTML();
                    break;

                    case 'produitList':
                    $this->ctrlProduitList->getHTML();
                    break;

                    case 'tunnel':
                    $this->ctrlTunnel->getHTML();
                    break;

                    case 'inscription':
                    $this->ctrlInscription->registerUser();
                    break;

                    case 'adminProduit':
                    $this->ctrlAdminProduit->getHTML();
                    break;

                    case 'adminUser':
                    $this->ctrlAdminUser->handlerAdministrationUser();
                    break;

                    case 'adminPaiementLivraison':
                    $this->ctrlAdminPaiementLivraison->handlerPaiementLivraison();
                    break;

                    case 'login':
                    $this->ctrlLogin->getHTML();
                    break;

                    case 'deconnexion':
                    $this->ctrlLogin->logOut();
                    break;

                    case 'logguer':
                    $this->ctrlLogin->logguerUser();
                    break;

                    case 'recherche':
                    $this->ctrlRecherche->getHTML();
                    break;

                    case 'adminChiffreAffaire':
                    $this->ctrlChiffreAffaire->getHTML();
                    break;

                    case 'adminHistoriqueCommande':
                    $this->ctrlAdminHistoriqueCommande->handlerHistoriqueCommande();
                    break;

                    case 'faq':
                    $this->ctrlFAQ->handlerFAQ();
                    break;

                    case 'Music':
                    $this->ctrlMusic->getHTML();
                    break;

                    case 'Intro':
                    $this->ctrlIntro->getHtml();
                    break;

                    default:
                        throw new Exception("Action non valide");
                }
            } else {  // aucune action définie : affichage de l'accueil
                $this->ctrlAccueil->getHTML();
            }
        } catch (Exception $e) {
            $this->erreur($e->getMessage());
        }
    }


    /**
     * Fonction qui affiche une erreur
     *
     * @param $msgErreur
     */
    private function erreur($msgErreur)
    {
        $vue = new Vue("Erreur");
        $vue->generer(array('msgErreur' => $msgErreur));
    }
}