<?php

require_once('Modele.php');


class AdministrationUser extends Modele
{

    /**
     * Renvoie les informations sur un utillisateur
     */
    public function getUser($userID)
    {
        $sql = 'SELECT userID AS id, nom, prenom, chemin, niveau_accreditation, mail, mot_de_passe FROM user WHERE userID=?';
        $user = $this->executerRequete($sql, array($userID));
        if ($user->rowCount() == 1)
            return $user->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun utilisateur ne correspond à l'identifiant '$userID'");
    }


    /**
     * Fonction qui renvoie la liste des utilisateurs.
     */
    public function getUserList()
    {
        $sql = "SELECT * FROM user";
        $user = $this->executerRequete($sql);
        return $user;
    }


    /**
     * Fonction qui met à jour le niveau d'accréditation d'un utilisateur
     */
    public function updateUserStatus($userID, $level)
    {
        $sql = "UPDATE user SET niveau_accreditation = ? WHERE userID = ?";
        $this->executerRequete($sql, array($level, $userID));
    }


    /**
     * Fonction qui supprime un utilisateur de la base de données
     */
    public function deleteUser($userID)
    {
        $sql = "DELETE FROM user WHERE userID = ?";
        $this->executerRequete($sql, array($userID));
    }
}

?>