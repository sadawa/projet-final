<?php


abstract class Modele
{

    /**
     * @var Objet PDO d'accès à la BD
     */
    private $bdd;


    //______________________________________________________________________________________
    /**
     * Renvoie un objet de connexion à la BD en initialisant la connexion au besoin
     *
     *  objet PDO de connexion à la BDD
     */
    private function getBdd()
    {
        if ($this->bdd == null) {
            // Création de la connexion
            $this->bdd = new PDO('mysql:host=sql310.epizy.com; dbname=epiz_26271722_geekanime	; charset=utf8', 'epiz_26271722', 'GQiKdb5hwqpIXJA
            ')
;
        }
        return $this->bdd;
    }


    /**
     * Exécute une requête SQL éventuellement paramétrée
     *
     * $sql La requête SQL
     * $valeurs Les valeurs associées à la requête
     * Le résultat renvoyé par la requête
     */
    protected function executerRequete($sql, $params = null)
    {

        if ($params == null) {
            $resultat = $this->getBdd()->query($sql); // exécution directe
        } else {
            $resultat = $this->getBdd()->prepare($sql);  // requête préparée
            $resultat->execute($params);
        }
        return $resultat;
    }
}