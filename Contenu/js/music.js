
/* Ici on va utlise le jquery pour lancer les music dans page vueMusic du coup on commence
a mettre on forme jquery*/
$(document).ready(function () {
    var playing = false,
        artistname = $(".artist-name"),
        musicName = $(".music-name"),
        time = $(".time"),
        fillBar = $(".fillBar");
/* On utilise un tableau pour stocker les msuic dans le dossier music sa va nous servir 
pour cree la playliste */
    let audioData = [];

    var song = new Audio();
    var CurrentSong = 0;
    window.onload = load();

    function load() {

        audioData = [
            {
                name: "never-have-i-felt-this",
                artist: "Koven",
                src:
                    "Music/koven-never-have-i-felt-this-ncs-release.mp3",
            },
            {
                name: "a life",
                artist: "rude",
                src:
                    "Music/dr-stone-ending-fullrude-a-life.mp3",
            },
            {
                name: "cool-blue-light",
                artist: "",
                src: "Music/kronicle-cool-blue-light-no-copyright-background-music.mp3",
            },
            {
                name: "BTOOOM",
                artist: "",
                src: "Music/BTOOOM! OST - 1 BTOOOM! (Download Link).mp3",
            },
            {
                name: "ffxiv theme shadow",
                artist: "Square enix",
                src: "Music/final-fantasy-xiv-shadowbringers-ost-shadowbringers-main-theme-msq-spoilers.mp3",
            },
            {
                name: "talk fly away",
                artist: "ncs krys",
                src: "Music/krys-talk-fly-away-ncs-release.mp3",
            },
            {
                name: "just-you-and-me",
                artist: "mndlss-feat-sousa",
                src: "Music/mndlss-feat-sousa-perth-just-you-and-me.mp3",
            },
            {
                name: "murakami",
                artist: "",
                src: "Music/murakami.mp3",
            },
            {
                name: "san-francisco",
                artist: "-midicronica",
                src: "Music/samurai-champloo-san-francisco-ending-of-episode-26-midicronica.mp3",
            },
            {
                name: "luv-sic-pt-3",
                artist: "nujabesftshing02 ",
                src: "Music/nujabes-luv-sic-pt-3-ftshing02.mp3",
            },
            {
                name: "street-fighter-5-main-menu",
                artist: "Capcom ",
                src: "Music/street-fighter-5-main-menu-music.mp3",
            },
            {
                name: "we-are-heroes",
                artist: "johannes-bornlof ",
                src: "Music/we-are-heroes-1-by-johannes-bornlof-action-music.mp3",
            },
            {
                name: "you",
                artist: "kazami ",
                src: "Music/you-ft-kazami-samurai-champloo-ep-17-ending-theme-full-credits.mp3",
            },
            {
                name: "goblin-slayer-ost-maintheme",
                artist: " ",
                src: "Music/goblin-slayer-ost-maintheme.mp3",
            },  
            {
                name: "sh0ut",
                artist: "hiroyuki",
                src: "Music/sh0ut.mp3",
            },
            {
                name: "rottengrafft ending dragon ball super ",
                artist: "70cm",
                src: "Music/rottengraffty-70cmmusic-video.mp3",
            },
            {
                name: "bulls-in-the-bron ",
                artist: "pierce-the-veil",
                src: "Music/pierce-the-veil-bulls-in-the-bronx-by-lyrics.mp3",
            },  
            {
                name: "pianophonic",
                artist: "pax-japonica-groove",
                src: "Music/pax-japonica-groove-pianophonic.mp3",
            },
            {
                name: "psycho pass ost",
                artist: "",
                src: "Music/psycho-pass-ost-1.mp3",
            },  
        ];

        //console.log(audioData);
    // on stock le tableau avec le lien dans une variable et on utilise des DOM 
        artistname.html(audioData[CurrentSong].artist);
        musicName.html(audioData[CurrentSong].name);
        song.src = (audioData[CurrentSong].src);
    }
// la function a pour but de stock la cle du tableau et le chemin src et faire du jquery 
    function playSong() {
        var curSong = audioData[CurrentSong];
        artistname.html(curSong.artist);
        musicName.html(curSong.name);
        song.src = (curSong.src);
        song.play();
        $("#play").addClass("fa-pause");
        $("#play").removeClass("fa-play");
        $("img").addClass("active");
        $(".player-track").addClass("active")
    }
// la fonction a pour but de mettre temps de la music 
    song.addEventListener("timeupdate", function() {
        
        var position = (100 / song.duration) * song.currentTime;
        var current = song.currentTime;
        var duration = song.duration;
        var durationMinute = Math.floor(duration / 60);
        var durationSecond = Math.floor(duration - durationMinute * 60);
        var durationLabel = durationMinute + ":" + durationSecond; 
            currentSecond = Math.floor(current);
            currentMinute = Math.floor(currentSecond / 60);
            currentSecond = currentSecond - (currentMinute * 60);
        
             if(currentSecond < 10){
                 ( currentSecond = "0" + currentSecond);
             }
        var currentLabel = currentMinute + ":" + currentSecond;
        var indicatorLabel = currentLabel + " / " + durationLabel

        fillBar.css("width", position + '%');
        
        $(".time").html(indicatorLabel);
        
    });
/* Enfin on va faire des event pour lancer une music mettre en pause allez au suivant ou précedent */

    $("#play").click(function playOrPause() {
        if (song.paused) {
            song.play();
            playing = true;
            $("#play").addClass("fa-pause");
            $("#play").removeClass("fa-play");
            $("img").addClass("active");
        } else {
            song.pause();
            playing = false;
            $("#play").removeClass("fa-pause");
            $("#play").addClass("fa-play");
            $("img").removeClass("active");
        }
    });

    $("#prev").click(function prev() {
        CurrentSong--;
        if (CurrentSong < 0) {
            CurrentSong = 18;
        }
        playSong();
    });

    $("#next").click(function next() {
        CurrentSong++;
        if (CurrentSong > 18) {
            CurrentSong = 0;
        }
        playSong();
    });
});  
