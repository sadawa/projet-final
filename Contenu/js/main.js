const search = document.getElementById("search");
const liste = document.getElementById("liste");

// on va recherche liste.json et le filtré
const searchliste = async (searchText) => {
  const res = await fetch("Contenu/data/liste.json");
  const produit = await res.json();
  // console.log(produit);

  // on va filtré quand utilisateur va écrire dans le input
  let matches = produit.filter((produit) => {
    const regex = new RegExp(`^${searchText}`, "gi");
    return produit.name.match(regex);
  });
  if (searchText.length === 0) {
    matches = [];
    liste.innerHTML = "";
  }
  outputhtml(matches);
};

// On va montre le resultat dans le html
const outputhtml = (matches) => {
  if (matches.length > 0) {
    const html = matches
      .map(
        (match) => `
    <div class='card card-body mb-1'>
    <h4>${match.name}</h4>
    </div>`
      )
      .join("");

    liste.innerHTML = html;

    //console.log(html)
  }
};

search.addEventListener("input", () => searchliste(search.value));
